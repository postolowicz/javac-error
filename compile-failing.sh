#!/usr/bin/env bash

javac \
  -source \
  1.8 \
  -target \
  1.8 \
  -d \
  system-test/build/classes/main \
  -g \
  -sourcepath \
  -proc:none \
  -classpath \
  common-test/build/libs/common-test.jar \
  system-test/src/main/java/postolowicz/systemtest/AbstractSystemTestWithoutDB.java \
  system-test/src/main/java/postolowicz/systemtest/AbstractSystemTest.java
