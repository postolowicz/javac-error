package postolowicz.systemtest;

import static postolowicz.systemtest.AbstractSystemTest.*;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

import postolowicz.test.ValidationRunner;

public abstract class AbstractSystemTestWithoutDB
	{

	@Configuration
	@PropertySource(value = "", ignoreResourceNotFound = true)
	public static class TestConfig {
	}

	protected class RunnerImpl extends ValidationRunner {

		public RunnerImpl() {
		}

		@Override
		public String method2() {
			return SOME_STRING;
		}

		@Override
		protected String method1(String s) {
			return "";
		}
	}

}
