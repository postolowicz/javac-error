package postolowicz.test;

public abstract class ValidationRunner {

	public void method0(String s) throws Exception {
	}

	protected String method1(String s) {
		return s;
	}

	public abstract String method2();

}
